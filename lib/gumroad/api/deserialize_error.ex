defmodule Gumroad.Api.DeserializeError do
  defexception [:message]
end
