defmodule CodeStats.User.Flow.Utils do
  alias CodeStats.User.Flow

  @type flow_day_data() :: %{date: Date.t(), duration: non_neg_integer(), xp: integer()}

  @doc """
  Divide a flow to two days, in case it goes over the midnight into the next day.

  Assumptions:
  * No DST transition happens, we don't have info to detect that anyway
  * No flow is longer than 24 hours
  * XP is accumulated evenly throughout the flow
  """
  @spec divide_flow_minutes(Flow.t()) :: {Flow.t(), Flow.t()}
  def divide_flow_minutes(flow) do
    utc_offset =
      flow.start_time_local
      |> DateTime.from_naive!("Etc/UTC")
      |> DateTime.diff(flow.start_time, :minute)

    start_day = NaiveDateTime.to_date(flow.start_time_local)
    next_day = Date.add(start_day, 1)
    next_start_of_day = NaiveDateTime.new!(next_day, ~T[00:00:00])
    diff_to_next_day = NaiveDateTime.diff(next_start_of_day, flow.start_time_local)
    minutes_start_day = min(div(diff_to_next_day, 60), flow.duration)
    minutes_next_day = max(flow.duration - minutes_start_day, 0)

    first_day_size = minutes_start_day / flow.duration
    next_day_size = minutes_next_day / flow.duration
    xp_first_day = round(flow.xp * first_day_size)
    xp_next_day = round(flow.xp * next_day_size)

    next_start_of_day_utc =
      next_start_of_day
      |> NaiveDateTime.add(-utc_offset, :minute)
      |> DateTime.from_naive!("Etc/UTC")

    {
      %Flow{
        start_time: flow.start_time,
        start_time_local: flow.start_time_local,
        duration: minutes_start_day,
        xp: xp_first_day
      },
      %Flow{
        start_time: next_start_of_day_utc,
        start_time_local: next_start_of_day,
        duration: minutes_next_day,
        xp: xp_next_day
      }
    }
  end
end
