defmodule CodeStatsWeb.ViewUtils do
  @moduledoc """
  View functions and utilities common to most views.
  """

  @doc """
  Format amount of XP for display.
  """
  @spec format_xp(number) :: String.t()
  def format_xp(xp) do
    Number.Delimit.number_to_delimited(xp, precision: 0)
  end

  @doc """
  Format given duration (in minutes) to a human readable duration.
  """
  @spec human_duration(pos_integer()) :: String.t()
  def human_duration(minutes) do
    hours = div(minutes, 60)
    minutes = minutes - hours * 60

    [
      if(hours > 0, do: "#{hours} h"),
      if(hours == 0 or minutes > 0, do: "#{minutes} m")
    ]
    |> Enum.filter(&(not is_nil(&1)))
    |> Enum.join(" ")
  end
end
