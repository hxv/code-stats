defmodule CodeStatsWeb.ProfileLive.DataProviders.UserInfo do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      username: String.t(),
      gravatar_email: String.t() | nil,
      inserted_at: DateTime.t()
    })
  end

  @impl true
  def required_data(), do: MapSet.new()

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(_shared_data, user) do
    %Data{
      username: user.username,
      gravatar_email: user.gravatar_email,
      inserted_at: user.inserted_at
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(data, _user, _pulse, _cache), do: data
end
