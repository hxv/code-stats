defmodule Gumroad.Api.Utils do
  require Logger
  alias CodeStats.HTTPClient
  alias Gumroad.Api.LicenseResponse

  @doc """
  Build a generic request to send to the Gumroad API.
  """
  @spec build_request(
          Finch.Request.method(),
          Finch.Request.url(),
          Finch.Request.headers(),
          Keyword.t(),
          Keyword.t()
        ) :: Finch.Request.t()
  def build_request(method, url, headers \\ [], body \\ [], opts \\ []) do
    body =
      if method in [:post, :put, :patch] do
        body
        |> Keyword.put(:access_token, Application.fetch_env!(:code_stats, :gumroad_access_token))
        |> URI.encode_query()
      else
        nil
      end

    headers =
      List.keystore(
        headers,
        "content-type",
        0,
        {"content-type", "application/x-www-form-urlencoded"}
      )

    Finch.build(method, url, headers, body, opts)
  end

  @doc """
  Verify the validity of a license key for the given product.
  """
  @spec verify_license(String.t(), String.t()) :: {:ok, LicenseResponse.t()} | :not_found | :error
  def verify_license(product_permalink, license_key) do
    verify_req =
      build_request(:post, Application.fetch_env!(:code_stats, :gumroad_license_url), [],
        product_permalink: product_permalink,
        license_key: license_key,
        increment_uses_count: "false"
      )

    Appsignal.increment_counter("gumroad_license_verify")

    with {:ok, resp} <- Finch.request(verify_req, HTTPClient),
         200 <- resp.status,
         {:ok, body} <- Jason.decode(resp.body) do
      Appsignal.increment_counter("gumroad_license_verify_success")
      {:ok, LicenseResponse.deserialize(body)}
    else
      404 ->
        Appsignal.increment_counter("gumroad_license_verify_not_found")
        :not_found

      {:error, error} ->
        Appsignal.increment_counter("gumroad_license_verify_error")
        Logger.error("Error verifying license from Gumroad: #{inspect(error)}")
        :error

      status ->
        Appsignal.increment_counter("gumroad_license_verify_error_unacceptable_status")

        Logger.error(
          "Unacceptable status code when verifying license from Gumroad: #{inspect(status)}"
        )

        :error
    end
  end
end
