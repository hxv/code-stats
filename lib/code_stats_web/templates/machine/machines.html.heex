<div class="stripe">
  <h1>Configured machines</h1>

  <div class="machine-list">
    <%= for %{active: true} = machine <- @machines do %>
      <div class="machine">
        <h2><%= machine.name %></h2>
        <div class="api-key-text">
          <span>API key:</span>
          <input
            type="text"
            class="api-key"
            readonly
            value={CodeStatsWeb.AuthUtils.get_machine_key(@conn, @user, machine)}
          />
        </div>

        <button type="button" class="copy-button">Copy</button>

        <%= form_tag(Routes.machine_path(@conn, :view_single, machine.id), method: :get, class: "edit-form") do %>
          <button type="submit">Edit</button>
        <% end %>

        <%= form_tag(Routes.machine_path(@conn, :regen_machine_key, machine.id), class: "regenerate-form") do %>
          <button type="submit" class="button-warning machine-regenerate-button">
            Regenerate API key
          </button>
        <% end %>

        <%= form_tag(Routes.machine_path(@conn, :deactivate, machine.id), class: "deactivate-form") do %>
          <button type="submit" class="button-danger machine-deactivate-button">
            Deactivate
          </button>
        <% end %>

        <%= form_tag(Routes.machine_path(@conn, :delete, machine.id), method: :delete, class: "delete-form") do %>
          <button type="submit" class="button-danger machine-delete-button">Delete</button>
        <% end %>
      </div>
    <% end %>
  </div>

  <%= if Enum.empty?(@machines) do %>
    <div class="alert alert-info">
      You don't have any machines configured. :( You should add one below!
    </div>
  <% else %>
    <div class="alert alert-warning">
      Note that if you regenerate a machine's API key, the old API key will stop working immediately. You should do this if you lose your API key or the machine that was connected to it.
    </div>
  <% end %>

  <h2>Add new machine</h2>

  <%= form_for(@changeset, Routes.machine_path(@conn, :list), fn f -> %>
    <label for="machine_name" class={if f.errors[:name], do: "has-error", else: ""}>
      Name (required)
    </label>

    <div class="input">
      <%= text_input(f, :name, required: true, maxlength: machine_name_max_length()) %>
      <.form_error form={f} field={:name} />
    </div>

    <div class="submit"><%= submit("Add machine") %></div>
  <% end) %>

  <%= if Enum.any?(@machines, &match?(%{active: false}, &1)) do %>
    <div class="inactive-machine-list">
      <h2>Inactive machines</h2>
      <%= for %{active: false} = machine <- @machines do %>
        <%= form_tag(Routes.machine_path(@conn, :activate, machine.id), class: "machine") do %>
          <div class="machine-name"><%= machine.name %></div>
          <button type="submit">Activate</button>
        <% end %>
      <% end %>
    </div>
  <% end %>
</div>
