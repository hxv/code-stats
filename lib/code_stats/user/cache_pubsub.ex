defmodule CodeStats.User.Cache.PubSub do
  @moduledoc """
  System to retrieve live updates from whenever a user's cache is regenerated.
  """

  @spec subscribe(pos_integer()) :: :ok
  def subscribe(id) do
    {:ok, _} = Registry.register(__MODULE__, id, nil)
    :ok
  end

  @spec publish(CodeStats.User.t(), CodeStats.User.Cache.t()) :: :ok
  def publish(user, cache) do
    Registry.dispatch(__MODULE__, user.id, fn subscribers ->
      for {pid, _} <- subscribers do
        send(pid, {__MODULE__, :cache, cache})
      end
    end)
  end
end
